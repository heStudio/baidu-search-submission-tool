#!/usr/bin/env python

from distutils.core import setup

setup(name='hbsst',
      version='1.0.1',
      description='本工具将百度站长API提交的命令集成优化，改成利于使用的Python函数，方便程序调用。',
      author='heStudio',
      author_email='hestudio@hestudio.org',
      url='https://gitee.com/heStudio/baidu-search-submission-tool/',
      packages=[''],
     )
